#####################################################
# VPC - AWS #
#####################################################
provider "aws"{
  region               = "ap-southeast-1"
  access_key           = ""
  secret_key           = ""

}

# Create VPC
resource "aws_vpc" "prod-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "Production"   
  }
}

# Create Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod-vpc.id

 }

#####################################################
# Public Subnet & Private Subnet - Inbound/Outbound Internet Access #
#####################################################

/* Routing table for private subnet */

resource "aws_route_table" "private" {
  vpc_id = aws_vpc.prod-vpc.id
  
  tags = {
    Name = "prod_subnet_private"
  }
}

/* Routing table for public subnet */

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.prod-vpc.id
  
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  } 

  tags = {
    Name = "prod_subnet_public"
  }

}

# Create a Public Subnet

resource "aws_subnet" "public_subnet" {
  cidr_block              = "10.0.1.0/24"
  vpc_id                  = aws_vpc.prod-vpc.id
  availability_zone       = "ap-southeast-1a"

  tags = {
    Name = "prod_subnet_public"
  }
}

# Create a Private Subnet

resource "aws_subnet" "private_subnet" {
  cidr_block              = "10.0.2.0/24"
  vpc_id                  = aws_vpc.prod-vpc.id
  availability_zone       = "ap-southeast-1a"

  tags = {
    Name = "prod_subnet_private"
  }
}

/* NAT and EIP to NAT */
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.one.id
  subnet_id     = aws_subnet.public_subnet.id
  depends_on    = [aws_internet_gateway]
  
  tags = {
    Name        = "nat"
  }
}

/* NAT for Private Subnet */

resource "aws_subnet" "private_subnet" {
  route_table_id         = "aws_route_table" "public"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

# Create Associate subnet with Route table

/* Route table associations */

resource "aws_route_table_association" "public" {
  subnet_id               = aws_subnet.public_subnet.id
  route_table_id          = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  subnet_id               = aws_subnet.private_subnet.id
  route_table_id          = aws_route_table.private.id
}


# Create a security group to allow port 80,22,443
resource "aws_security_group" "allow web" {
  name                    = "allow_web_traffic"
  description             = "Allow web inbound traffic"
  vpc_id                  = "aws_vpc.prod-vpc.id"
  depends_on              = [aws_vpc.prod-vpc.id]

  ingress {
    description           = "HTTP"
    from_port             = 80
    to_port               = 80
    protocol              = "tcp"
    cidr_blocks           = ["0.0.0.0/0"]
  }

  ingress {
    description           = "HTTPS"
    from_port             = 445
    to_port               = 443
    protocol              = "tcp"
    cidr_blocks           = ["0.0.0.0/0"]
  }
  ingress {
    description           = "SSH  "
    from_port             = 22
    to_port               = 22
    protocol              = "tcp"
    cidr_blocks           = ["0.0.0.0/0"]
  }
  egress { 
    from_port             = 0
    to_port               = 0
    protocol              = "-1"
    cidr_blocks           = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web"
 }

}
 
# Create a Network Interface with an ip in the subnet that was created in step 4
resource "aws_network_interface" "web-server-nic" {  
  subnet_id       = aws_subnet.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]
} 
# Assign an elastic IP to the network interface created in step 7

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server-nic
  associate_with_pivate_ip  = "10.0.1.50"
  depends_on                = [aws_internet_gateway]
}

# Create Ubuntu server and install/enable apache2

resource "aws_instance" "web-server-instance" {
  ami                  = "ami-93431h9134h34901"
  instance_type        = "t2.micro"
  availability_zone    = "ap-southeast-1a"
  key_name             = "key_name"   # Create the key before run this script

  network_interface {
    device_index          = 0
    network_interface_id  = aws_network_interface.web-server-nic.id
  }

  user_data   = <<-EOF
              #!/bin/bash
              sudo apt update -y 
              sudo apt install nginx
              sudo bash -c 'echo your very first web server > /var/www/html/index.html'
              EOF
  tags = {
    Name = "web-server"
  }                           
}

## Resource aws_network_interface with an IP address that was created in steps 4
 # security_groups = [aws_security_group.allow_web.id]
#
#
## 
 # attachment {
##    instance     = aws_instance.test.id
##    device_index = 1   
##  }
##}
##
##
##
##}
##resource "aws_route_table" "public_a" {
##  vpc_id = aws_vpc.prod-vpc.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-a")
##  )
##}
##
##resource "aws_route_table_association" "public_a" {
##  subnet_id      = aws_subnet.public_a.id
##  route_table_id = aws_route_table.public_a.id
##}
##
##resource "aws_route" "public_internet_access_a" {
##  route_table_id         = aws_route_table.public_a.id
##  destination_cidr_block = "0.0.0.0/0"
##  gateway_id             = aws_internet_gateway.prod-vpc.id
##}
##
##resource "aws_eip" "public_a" {
##  vpc = true
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-a")
##  )
##}
##
##resource "aws_nat_gateway" "public_a" {
##  allocation_id = aws_eip.public_a.id
##  subnet_id     = aws_subnet.public_a.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-a")
##  )
##}
##
##resource "aws_subnet" "public_b" {
##  cidr_block              = "10.1.2.0/24"
##  map_public_ip_on_launch = true
##  vpc_id                  = aws_vpc.prod-vpc.id
##  availability_zone       = "${data.aws_region.current.name}b"
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-b")
##  )
##}
##
##resource "aws_route_table" "public_b" {
##  vpc_id = aws_vpc.prod-vpc.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-b")
##  )
##}
##
##resource "aws_route_table_association" "public_b" {
##  subnet_id      = aws_subnet.public_b.id
##  route_table_id = aws_route_table.public_b.id
##}
##
##resource "aws_route" "public_internet_access_b" {
##  route_table_id         = aws_route_table.public_b.id
##  destination_cidr_block = "0.0.0.0/0"
##  gateway_id             = aws_internet_gateway.prod-vpc.id
##}
##
##resource "aws_eip" "public_b" {
##  vpc = true
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-b")
##  )
##}
##
##resource "aws_nat_gateway" "public_b" {
##  allocation_id = aws_eip.public_b.id
##  subnet_id     = aws_subnet.public_b.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-public-b")
##  )
##}
##
####################################################
### Private Subnets - Outbound internt access only #
####################################################
##
##resource "aws_subnet" "private_a" {
##  cidr_block        = "10.1.10.0/24"
##  vpc_id            = aws_vpc.prod-vpc.id
##  availability_zone = "${data.aws_region.current.name}a"
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-private-a")
##  )
##}
##
##resource "aws_route_table" "private_a" {
##  vpc_id = aws_vpc.prod-vpc.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-private-a")
##  )
##}
##
##resource "aws_route_table_association" "private_a" {
##  subnet_id      = aws_subnet.private_a.id
##  route_table_id = aws_route_table.private_a.id
##}
##
##resource "aws_route" "private_a_internet_out" {
##  route_table_id         = aws_route_table.private_a.id
##  nat_gateway_id         = aws_nat_gateway.public_a.id
##  destination_cidr_block = "0.0.0.0/0"
##}
##
##resource "aws_subnet" "private_b" {
##  cidr_block        = "10.1.11.0/24"
##  vpc_id            = aws_vpc.prod-vpc.id
##  availability_zone = "${data.aws_region.current.name}b"
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-private-b")
##  )
##}
##
##resource "aws_route_table" "private_b" {
##  vpc_id = aws_vpc.prod-vpc.id
##
##  tags = merge(
##    local.common_tags,
##    map("Name", "${local.prefix}-private-b")
##  )
##}
##
##resource "aws_route_table_association" "private_b" {
##  subnet_id      = aws_subnet.private_b.id
##  route_table_id = aws_route_table.private_b.id
##}
##
#resource "aws_route" "private_b_internet_out" {
#  route_table_id         = aws_route_table.private_b.id
#  nat_gateway_id         = aws_nat_gateway.public_b.id
#  destination_cidr_block = "0.0.0.0/0"
#}
#
##################################################
# Autoscaling Group - CPU Utilization #
##################################################

module "autoscale_group" {
  source = "cloudposse/ec2-autoscale-group/aws"

  image_id                    = "ami-08cab282f9979fc7a"
  instance_type               = "T2.medium"
  subnet_ids                  = ["aws_subnet-private_b"]
  health_check_type           = "EC2"
  min_size                    = 2
  max_size                    = 5
  wait_for_capacity_timeout   = "5m"
  associate_public_ip_address = true
  user_data_base64            = base64encode(local.userdata)

  # Auto-scaling policies and CloudWatch metric alarms
  autoscaling_policies_enabled           = true
  cpu_utilization_high_threshold_percent = "45"
  cpu_utilization_low_threshold_percent  = "20"
}

